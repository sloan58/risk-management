<?php

return [

    'impact' => [
        'High',
        'Moderate',
        'Low'
    ],

    'status' => [
        'Ongoing',
        'Pending',
        'Completed'
    ],

    'assignment' => [
        'Engineering',
        'Management',
        'Vendor'
    ]
];