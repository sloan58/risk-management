<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNistControlNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nist_control_names', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->integer('nist_control_id')->unsigned();
            $table->foreign('nist_control_id')->references('id')->on('nist_controls');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nist_control_names');
    }
}
