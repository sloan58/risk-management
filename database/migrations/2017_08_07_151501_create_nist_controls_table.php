<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNistControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nist_controls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('family');
            $table->string('title');
            $table->string('priority');
            $table->string('baseline_impact');
            $table->text('description');
            $table->text('supplemental_guidance');
            $table->text('related');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nist_controls');
    }
}
