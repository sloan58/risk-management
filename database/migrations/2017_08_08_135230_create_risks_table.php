<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRisksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tracking_number');
            $table->string('title');
            $table->text('description');
            $table->dateTime('date_identified');
            $table->string('impact');
            $table->string('status');
            $table->dateTime('target_completion');
            $table->text('fix_action')->nullable();
            $table->string('created_by');
            $table->string('assignment');
            $table->integer('nist_control_name_id')->unsigned();
            $table->foreign('nist_control_name_id')->references('id')->on('nist_control_names');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('risks');
    }
}
