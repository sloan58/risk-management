<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Risk::class, function (Faker\Generator $faker) {

    $control = \App\Models\NistControl::inRandomOrder()->first();

    return [
        'tracking_number' => '2017-A-' . $faker->unique()->numberBetween(1,100),
        'nist_control_name_id' => $control->nistControlNames()->inRandomOrder()->first()->id,
        'title' => $faker->sentence(),
        'description' => $faker->paragraph(),
        'date_identified' => $faker->dateTimeBetween('-10 Months', '-1 Month'),
        'impact' => $faker->randomElement(config('enums.impact')),
        'status' => $faker->randomElement(config('enums.status')),
        'target_completion' => $faker->dateTimeBetween('-2 Months', '+6 Months'),
        'fix_action' => $faker->paragraph(),
        'assignment' =>$faker->randomElement(config('enums.assignment')),
        'created_by' => \App\User::inRandomOrder()->first()->name,
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$autoIncrement = autoIncrement();
$factory->define(App\Models\Milestone::class, function (Faker\Generator $faker) use ($autoIncrement){

    $autoIncrement->next();

    return [
        'body' => $faker->paragraph(),
        'created_by' => \App\User::inRandomOrder()->first()->name,
        'priority' =>  $autoIncrement->current(),
        'completed' => false,
        'risk_id' => \App\Models\Risk::inRandomOrder()->first()->id,
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Comment::class, function (Faker\Generator $faker) use ($autoIncrement){

    return [
        'body' => $faker->paragraph(),
        'created_by' => \App\User::inRandomOrder()->first()->name,
        'risk_id' => \App\Models\Risk::inRandomOrder()->first()->id,
    ];
});


function autoIncrement() {
    for ($i = 0; $i <=6; $i++) {
        if($i >= 6) $i = 1;
        yield $i;
    }
};