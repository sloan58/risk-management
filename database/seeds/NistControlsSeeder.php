<?php

use App\Models\NistControl;
use App\Models\NistControlName;
use Illuminate\Database\Seeder;

class NistControlsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get the NIST 800-53 Controls file from the NVD Website
        \Storage::put('800-53.txt', file_get_contents('https://nvd.nist.gov/static/feeds/xml/sp80053/rev4/800-53-controls.txt'));

        // Open the new file
        $fp = fopen(storage_path('app/800-53.txt'),'r');

        // Move past the file headers
        fgetcsv($fp, 0, "\t");

        $newRecord = [];
        $names = [];

        // Iterate the file content
        while (($line = fgetcsv($fp, 0, "\t")) !== FALSE) {

            // If $line[0] exists and $newRecord['family'] is not set
            // then this is a new Control record
            if($line[0] && ! isset($newRecord['family'])) {

                // Extract the record details and start building
                // a list of Control Names for the database
                list($newRecord, $names) = $this->startNewRecord($line);

                // If $line[0] exists and $newRecord['family'] exists
                // then we've hit the end of building the Control
                // Names and descriptions.  Let's save the record
            } elseif($line[0] && isset($newRecord['family'])) {

                // Save the new record to the database
                $this->saveNewNistControl($newRecord, $names);

                // Clear out the $newRecord to start over
                unset($newRecord);

                // Start a new record
                list($newRecord, $names) = $this->startNewRecord($line);

                // We're in the middle of building the record
                // Continue building Control Names and descriptions
            } else {
                array_push($names,$line[1]);
                $newRecord['description'] =  $newRecord['description'] . ' ' . $line[5];

            }
        }

        $this->saveNewNistControl($newRecord, $names);

        // Close the file
        fclose($fp);

        // Delete the file
        Storage::delete('app/800-53.txt');
    }

    /**
     * @param $line
     * @return mixed
     */
    protected function startNewRecord($line)
    {
        $newRecord['family'] = $line[0];
        $names = [];
        array_push($names,$line[1]);
        $newRecord['title'] = $line[2];
        $newRecord['priority'] = $line[3];
        $newRecord['baseline_impact'] = $line[4];
        $newRecord['description'] = $line[5];
        $newRecord['supplemental_guidance'] = $line[6];
        $newRecord['related'] = $line[7];
        return [$newRecord, $names];
    }

    /**
     * @param $newRecord
     * @param $names
     */
    protected function saveNewNistControl($newRecord, $names)
    {
        $nistControl = NistControl::create($newRecord);

        foreach ($names as $name) {
            $nistControlName = new NistControlName([
                'name' => $name
            ]);
            $nistControl->nistControlNames()->save($nistControlName);
        }
    }
}
