<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\Artisan::call('migrate:refresh');

        \App\User::insert([
            [
                'name' => 'Marty',
                'email' => 'martinsloan58@gmail.com',
                'password' => bcrypt('adminadmin')
            ],
            [
                'name' => 'System Admin',
                'email' => 'martin.sloan@karma-tek.com',
                'password' => bcrypt('adminadmin')
            ],
            [
                'name' => 'Mason',
                'email' => 'mason_nguyen@ao.uscourts.gov',
                'password' => bcrypt('adminadmin')
            ]
        ]);

        factory('App\User', 10)->create();

        $this->call(NistControlsSeeder::class);
        $this->call(SpreadSheetSeeder::class);

//        $risks = factory('App\Models\Risk', 50)->create();
//
//        $risks->each(function($risk) {
//            factory('App\Models\Milestone', 5)->create([
//                'risk_id' => $risk->id,
//            ]);
//        });
//
//        $risks->each(function($risk) {
//            factory('App\Models\Comment', 20)->create([
//                'risk_id' => $risk->id,
//            ]);
//        });
    }
}
