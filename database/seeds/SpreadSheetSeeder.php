<?php

use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class SpreadSheetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::load(storage_path('app/NIPT_Risk_Mgmt_NoPass.xlsx'), function($reader) {

            // Getting all results
            $results = $reader->get();

            foreach($results as $row) {

                $risk = new \App\Models\Risk();

                $this->buildRiskTopLevelData($row, $risk);

                $risk->save();

                $this->createRiskMilestones($row, $risk);
                $this->createRiskComments($row, $risk);

            }
        });
    }

    /**
     * @param $row
     * @param $risk
     */
    function buildRiskTopLevelData($row, $risk)
    {
        $risk->tracking_number = $row['trackingnumber'];

        $pieces = explode("\n", $row['vulnerability_description']);
        foreach ($pieces as $key => $piece) {
            if ($piece == "") {
                unset($pieces[$key]);
            }
        }

        if (isset($pieces[0])) {
            $risk->title = $pieces[0];
        }
        array_shift($pieces);

        $risk->description = implode("\n", $pieces);

        $risk->date_identified = $row['dateidentified'];

        $risk->impact = $row['risk_impact'];
        $risk->status = $row['vulnerability_status'];
//                $risk->target_completion = $row['target_completion_date'];
        $risk->target_completion = \Carbon\Carbon::now()->addMonth();

        switch ($row['vendor_eng_or_mgmt']) {
            case 'V':
                $risk->assignment = 'Vendor';
                break;
            case 'E':
                $risk->assignment = 'Engineering';
                break;
            case 'M':
                $risk->assignment = 'Management';
                break;
            default:
                $risk->assignemtn = '';
        }

        $safeGuard = preg_replace('/(\w{2}-\d{1,2})(\(\d+\))$/', '$1 $2', $row['safeguardnumbers']);
        $nistControl = \App\Models\NistControlName::where('name', $safeGuard)->first();

        if ($nistControl) {
            $risk->nist_control_name_id = $nistControl->id;
        }

        $risk->fix_action = $row['fix_action'];

        $risk->created_by = \App\User::find(2)->name;
    }

    /**
     * @param $row
     * @param $risk
     */
    function createRiskMilestones($row, $risk)
    {
        $pieces = explode("\n", $row['milestones']);
        foreach ($pieces as $piece) {
            if ($piece == "") continue;

            $milestone = new \App\Models\Milestone();
            $milestone->body = preg_replace('/^\d\.\s(.*)/', "$1", $piece);
            $milestone->created_by = \App\User::find(2)->name;
            $milestone->priority = $risk->fresh()->nextMilestonePriority();

            $risk->milestones()->save($milestone);
        }
    }

    /**
     * @param $row
     * @param $risk
     */
    function createRiskComments($row, $risk)
    {
        if($row['comments'] != "") {
            $comment = new \App\Models\Comment();
            $body = str_replace("\n", "<br/>", $row['comments']);
            $comment->body = $body;
            $comment->created_by = \App\User::find(2)->name;
            $risk->comments()->save($comment);
        }

//        $pieces = explode("\n", $row['comments']);
//        foreach ($pieces as $piece) {
//            if ($piece == "") continue;
//            $comment = new \App\Models\Comment();
//            $comment->body = preg_replace('/^\d\.\s(.*)/', "$1", $piece);
//            $comment->created_by = \App\User::find(2)->name;
//        }
    }

}
