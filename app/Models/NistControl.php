<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NistControl extends Model
{
    protected $guarded = [];

    /**
     *  A NistControl Has Many NistControlNames
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function nistControlNames()
    {
        return $this->hasMany(NistControlName::class);
    }
}
