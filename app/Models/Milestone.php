<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Milestone extends Model
{
    protected $guarded = [];

    /**
     *  A Milestone Belong To a User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'created_by', 'name');
    }

    /**
     *  A Milestone Belong To a Risk
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function risk()
    {
        return $this->belongsTo(Risk::class);
    }
}
