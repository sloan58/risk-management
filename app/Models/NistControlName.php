<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NistControlName extends Model
{
    protected $guarded = [];

    protected $with = [
        'nistControl'
    ];

    /**
     *  A NistControlName Belongs To a NistControl
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function nistControl()
    {
        return $this->belongsTo(NistControl::class);
    }
}
