<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Risk extends Model
{
    protected $fillable = [
        'title',
        'description',
        'tracking_number',
        'impact',
        'status',
        'target_completion',
        'fix_action',
        'nist_control_name_id',
        'created_by',
        'assignment',
    ];

    protected $with = [
        'milestones',
        'comments',
        'nistControlName'
    ];

    protected $dates = [
        'target_completion'
    ];

    /**
     *  A Risk Belongs To a User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'created_by', 'name');
    }

    /**
     *  A Risk Has Many Milestones
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function milestones()
    {
        return $this->hasMany(Milestone::class)->orderBy('priority', 'asc');
    }

    /**
     *  Get the next priority for this Risk, or set as first priority
     *
     * @return int
     */
    public function nextMilestonePriority()
    {
        return $this->milestones->count() ? $this->milestones->last()->priority + 1 : 1;
    }

    /**
     *  A Risk Has Many Comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class)->orderBy('created_at', 'desc');
    }

    /**
     *  A Risk Belongs To a NistControl
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function nistControlName()
    {
        return $this->belongsTo(NistControlName::class);
    }
}
