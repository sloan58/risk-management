<?php

namespace App\Http\Controllers;

use App\Models\Risk;
use App\Models\Comment;
use Illuminate\Http\Request;

class RiskCommentController extends Controller
{


    /**
     * RiskCommentController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Risk $risk)
    {
        $comment = new Comment([
            'body' => request()->get('body'),
            'created_by' => request()->get('created_by'),
        ]);

        $risk->comments()->save($comment);

        return $comment;
    }
}
