<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Risk;
use Illuminate\Http\Request;

class RiskController extends Controller
{
    /**
     * RiskController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('risks.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('risks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:risks|max:255',
            'description' => 'required',
            'tracking_number' => 'required',
            'impact' => 'required',
            'status' => 'required',
            'target_completion' => 'required',
            'fix_action' => 'required',
            'nist_control_name_id' => 'required',
            'created_by' => 'required',
        ]);

        $input = request()->all();

        $input['nist_control_name_id'] = $input['nist_control_name_id']['value'];

        try {
            $risk = Risk::create($input);
        } catch(\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return response([
            'risk' => $risk
        ], 200);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Risk  $risk
     * @return \Illuminate\Http\Response
     */
    public function show(Risk $risk)
    {
        return view('risks.show', compact('risk', $risk));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Risk  $risk
     * @return \Illuminate\Http\Response
     */
    public function edit(Risk $risk)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Risk  $risk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Risk $risk)
    {
        $risk->update(request()->all());

        return $risk;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Risk  $risk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Risk $risk)
    {
        //
    }
}
