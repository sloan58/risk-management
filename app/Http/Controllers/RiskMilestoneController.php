<?php

namespace App\Http\Controllers;

use App\Models\Risk;
use App\Models\Milestone;
use Illuminate\Http\Request;

class RiskMilestoneController extends Controller
{


    /**
     * RiskMilestoneController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Risk $risk)
    {
        $priority = $risk->nextMilestonePriority();

        $milestone = new Milestone([
            'body' => request()->get('body'),
            'completed' => false,
            'priority' => $priority,
            'created_by' => request()->get('created_by'),
        ]);

        $risk->milestones()->save($milestone);

        return $milestone;
    }

    public function update(Risk $risk)
    {
        $milestones = request()->all();

        foreach($milestones as $milestone) {
            if(Milestone::find($milestone['id'])) {
                Milestone::find($milestone['id'])->update($milestone);
            }
        }
    }

    public function destroy(Risk $risk, Milestone $milestone)
    {
        $milestone->delete();

        if(request()->expectsJson()) {
            return response(['status' => 'Reply Deleted']);
        }
        return back();
    }
}
