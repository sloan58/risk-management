<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \View::composer('*', function($view) {
            $impacts = config('enums.impact');
            $view->with('impacts', json_encode($impacts));
        });

        \View::composer('*', function($view) {
            $statuses = config('enums.status');
            $view->with('statuses', json_encode($statuses));
        });

        \View::composer('*', function($view) {
            $assignments = config('enums.assignment');
            $view->with('assignments', json_encode($assignments));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
