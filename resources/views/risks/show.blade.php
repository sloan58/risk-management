@extends('layouts.app')

@section('content')
    <risk
            :impacts="{{ $impacts }}"
            :statuses="{{ $statuses }}"
            :assignments="{{ $assignments }}"
            :risk="{{ $risk }}"
    ></risk>
@endsection
