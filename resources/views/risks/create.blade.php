@extends('layouts.app')

@section('content')
    <new-risk
            :impacts="{{ $impacts }}"
            :statuses="{{ $statuses }}"
            :assignments="{{ $assignments }}"
    ></new-risk>
@endsection
