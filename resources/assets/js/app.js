
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

Vue.prototype.authorize = function (handler) {

    let user = window.App.user;

    return user ? handler(user) : false;

};

window.events = new Vue();

window.flash = function (message, status) {
    window.events.$emit('flash', message, status);
};

window.moment = require('moment');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import vSelect from 'vue-select';

Vue.component('risk', require('./components/Risk.vue'));
Vue.component('risks', require('./components/Risks.vue'));
Vue.component('new-risk', require('./components/NewRisk.vue'));

Vue.component('vue2-filters', require('vue2-filters'));
Vue.component( 'v-select', vSelect );
Vue.component('flash', require('./components/Flash.vue'));
Vue.component('datepicker', require('vuejs-datepicker'));
Vue.component('draggable', require('vuedraggable'));
Vue.component('icon', require('vue-awesome'));
Vue.component('paginator', require('./components/Paginator.vue'));

const app = new Vue({
    el: '#app'
});
