<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('risks.index');
})->middleware('auth');

Auth::routes();

Route::get('/risks/export', function() {

    $excel = Maatwebsite\Excel\Facades\Excel::create('Risk_Tracker');

    $excel->sheet('Risks', function($sheet) {

            $sheet->row(1, [
                'Tracking Number',
                'Safeguard Number(s)',
                'Description',
                'Date Identified',
                'Risk Impact',
                'Vulnerability Status',
                'Target Completion Date',
                'Milestones',
                'POC',
                'Comments',
                'Vendor, Eng or Mgmt',
                'Fix Action'
            ]);

            $risks = \App\Models\Risk::all();

            $newRow = [];
            foreach ($risks as $risk) {
                array_push($newRow, $risk->tracking_number);
                array_push($newRow, $risk->nistControlName->name);
                array_push($newRow, $risk->title . "\n" . $risk->description);
                array_push($newRow, '');
                array_push($newRow, $risk->impact);
                array_push($newRow, $risk->status);
                array_push($newRow, $risk->target_completion->format('m/d/y'));

                if($milestones = $risk->milestones()->orderBy('priority', 'asc')->get()) {
                    $msRecord = '';
                    foreach($milestones as $milestone) {
                        $msRecord .= $milestone->priority . '. ' . $milestone->body . "\n";
                    }
                }

                array_push($newRow, $msRecord);
                $msRecord = '';

                array_push($newRow, '');

                if($comments = $risk->comments()->orderBy('created_at', 'asc')) {
                    $cRecord = '';
                    for($i=0; $i < $risk->comments->count(); $i++) {
                        $cRecord .= $i + 1 . str_replace("<br/>", "\n",$risk->comments[$i]->body) . "\n";
                    }
                }

                array_push($newRow, $cRecord);
                $cRecord = '';

                switch ($risk->assignment) {
                    case 'Vendor':
                        array_push($newRow, 'V');
                        break;
                    case 'Engineering':
                        array_push($newRow, 'E');
                        break;
                    case 'Management':
                        array_push($newRow, 'M');
                        break;
                    default:
                        $risk->assignemtn = '';
                }

                array_push($newRow, $risk->fix_action);

                $sheet->appendRow($newRow);
                $newRow = [];

            }
        });

    $lastrow = $excel->getActiveSheet()->getHighestRow();

    $excel->getActiveSheet()->getStyle('A1:L'.$lastrow)->getAlignment()->setWrapText(true);
    $excel->getActiveSheet()->setAutosize();

    return response()->download($excel->export('xlsx'));
});

Route::resource('risks', 'RiskController');
Route::post('/risks/{risk}/milestones', 'RiskMilestoneController@store');
Route::patch('/risks/{risk}/milestones', 'RiskMilestoneController@update');
Route::delete('/risks/{risk}/milestones/{milestone}', 'RiskMilestoneController@destroy');
Route::post('/risks/{risk}/comments', 'RiskCommentController@store');
