<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/risks', function() {

    if(request()->get('filter') == 'nist_control_name_id') {
        $risks = \App\Models\Risk::whereHas('nistControlName', function($q) {
            $q->where('name', 'like', '%' . request()->get('search') . '%');
        })
            ->orderBy('created_at', 'desc');
    } else {
        $risks = \App\Models\Risk::where(request()->get('filter'), 'like', '%' . request()->get('search') . '%')
            ->orderBy('created_at', 'desc');
    }

    return [
        'risks' => $risks->paginate(5)
    ];
});

Route::get('/nist-control-names/', function(\Illuminate\Http\Request $request) {

    if(request()->has('q') == '') {
        $results = \DB::table('nist_control_names')
            ->select('id as value', 'name as label')
            ->orderBy('label')
            ->take(10)
            ->get();
    } else {
        $results = \DB::table('nist_control_names')
            ->select('id as value', 'name as label')
            ->where('name', 'like', request()->get('q') . '%')
            ->orderBy('name')
            ->get();
    }


    return response()->json($results);

});